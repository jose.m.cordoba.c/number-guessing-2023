def dificulty_selector():
    while True:
        seleccion = int(input('''Selecciona la dificultad del juego
        1. Facil.
        2. Intermedia.
        3. Dificil
        '''))
        if seleccion == 1:
            return 20
        elif seleccion == 2:
            return 12
        elif seleccion == 3:
            return 5
        else:
            print('Seleccion invalida')

def game_play(secret, opportunities, player_1, player_2):
    print(f'El Jugador {player_1} tiene un numero secreto tienes {opportunities} intentos para encontrarlo ')
    guess = 0 
    for i in range(opportunities):
        guess = int(input(f'{player_2} dime el numero que consideras '))
        if secret == guess:
            print(f'Correcto el numero secreto es {secret}, adivinaste en {i + 1} intentos')
            break
        elif secret > guess:
            print(f'Lo siento {guess} es menor que el numero secreto, te quedan {opportunities - i - 1} intentos')
        else:
            print(f'Lo siento {guess} es mayor que el numero secreto, te quedan {opportunities - i - 1} intentos')
    if guess != secret:
        print(f'Se te acaberon tus {opportunities} intentos, el numero secreto era {secret} ')
    
    return {
        "guess_used": i+1,
        "guessed": guess == secret
    }


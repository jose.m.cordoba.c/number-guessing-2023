import openpyxl
import pandas as pd


WORKBOOK_NAME = "game_data.xlsx"

def add_game_data(player_1, player_2, game_type, secret, opportunities,	guess_use, guessed, date):
    # Cargar el libro de Excel existente
    workbook = openpyxl.load_workbook(WORKBOOK_NAME)

    # Seleccionar una hoja por su nombre
    sheet_name = "data"
    sheet = workbook[sheet_name]


    # Agregar una nueva fila
    new_row = [player_1, player_2, game_type, secret, opportunities, guess_use, guessed, date]
    sheet.append(new_row)

    # Guardar los cambios en el libro de Excel
    workbook.save(WORKBOOK_NAME)

def get_games_by_type():
    # Cargar el libro de Excel
    df = pd.read_excel(WORKBOOK_NAME)

    # Contar las apariciones de cada tipo de juego
    conteo_tipo_juego = df['tipo_juego'].value_counts()

    return conteo_tipo_juego

def contar_aciertos_por_jugador(jugador):
    # Cargar el libro de Excel
    df = pd.read_excel(WORKBOOK_NAME)

    # Filtrar el DataFrame por el jugador dado
    df = df[df['Jugador_2'] == jugador]

    # Contar las ocurrencias de True y False
    return df['adivinado'].value_counts()

def promedio_intentos():
    # Cargar el libro de Excel
    df = pd.read_excel(WORKBOOK_NAME)

    return df['intentos_utilizados'].mean()

def statistic_selector():
    while True:
        seleccion = int(input('''Selecciona la estadistica deseada
        1. Juegos por tipo.
        2. Resultados Por Jugador.
        3. Promedio Intentos utilizados
        4. Volver al Menu Principal
        '''))
        if seleccion == 1:
            print(f'Tipos de juego {get_games_by_type()}')
        elif seleccion == 2:
            player = input('Ingrese el nombre del Jugador ')
            print(f'Resultados para {player} {contar_aciertos_por_jugador(player)}')
        elif seleccion == 3:
            print(f'La media de intentos utilizados es de {promedio_intentos()}')
        elif seleccion == 4:
            return
        else:
            print('Seleccion invalida')


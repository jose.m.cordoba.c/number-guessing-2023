import game_logic as gl
import game_statistics
from random import randint
from getpass import getpass
from datetime import datetime


def __main__():

    # Menu
    opcion = 0;

    while opcion != 4:
        opcion = int(input('''Juego de adivinar seleccione una opción:
            1. Modo Solitario
            2. Partida dos Jugadores
            3. Estadistica
            4. Salir
            '''))
        
        if opcion == 1:
            secret = randint(1, 1000)
            name = input('Ingresa tu nombre ')
            opportunities = gl.dificulty_selector()
            data = gl.game_play(secret, opportunities, "Computer", name)
            game_statistics.add_game_data("cpu", name, "solitario", secret, opportunities, data["guess_used"], data["guessed"], datetime.now())

        elif opcion == 2:
            player_1 = input('Ingresa el nombre del jugador 1 ')
            secret = 0
            while secret < 1 or secret > 1000:
                secret = int(getpass(f'{player_1} Ingresa tu numero secreto seguido de intro '))
                if secret > 1000:
                    print("El numero debe ser menor o igual que 1000 ")
                if secret < 1:
                    print("El numero debe ser mayor o igual a 1")
            player_2 = input('Ingresa el nombre del jugador 2 ')
            opportunities = gl.dificulty_selector()
            data = gl.game_play(secret, opportunities, player_1, player_2)
            game_statistics.add_game_data(player_1, player_2, "dos_jugadores", secret, opportunities, data["guess_used"], data["guessed"], datetime.now())

        elif opcion == 3:
            game_statistics.statistic_selector()
        elif opcion == 4:
            print('Hasta luego')
        else:
            print('Opcion Invalida')

__main__()
